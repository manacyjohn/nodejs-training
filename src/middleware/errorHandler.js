exports.errorHandler = (err, req, res, next) => {
    const response = {
      code: err.status,
      message: err.message || httpStatus[err.status],
      errors: err.errors,
      stack: err.stack,
    };
  
    res.status(err.status);
    res.json(response);
    res.end();
};