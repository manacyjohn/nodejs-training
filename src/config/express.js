const express = require('express');
const bodyParser = require('body-parser');
const route = require('../routes');
const { errorHandler } = require('../middleware/errorHandler');

const app = express();

// parse body params and attach them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', route);

app.use(errorHandler)

module.exports = app;