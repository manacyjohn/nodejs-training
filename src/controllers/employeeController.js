const uuid = require('uuid').v4;
let employees = [];


exports.getEmployees = (req, res) => {
	let queries = Object.keys(req.query)
	if(queries.length>0){  
		if(queries.includes("dept")){
			console.log(req.query.dept)
			let employeesInDept = employees.filter(employee => {
				return employee.department === req.query.dept
			})
			res.send(employeesInDept)
		}

	}
	else{
		res.send(employees);
	}

};


exports.getEmployee = (req,res) => {
	const id = req.params.id
	console.log(id)
	let employee = employees.filter(employee => {
		return employee.id === id
	})

	res.send(employee)

};



exports.deleteEmployee = (req,res) => {
	const id = req.params.id
	let remainingEmployees = employees.filter(employee => {
		return employee.id !== id
  })
  employees=[...remainingEmployees]
	res.send(employees)

};



exports.createEmployee = (req, res) => {
  console.log(JSON.stringify(req.body));
  const employee = { ...req.body };
  employee.id = uuid();
  employees.push(employee);
  res.json(employee);
};


exports.updateEmployee = (req, res) => {
  console.log(JSON.stringify(req.body));
  id = req.params.id
  enteredDetails = {...req.body}
  employees.map(employee =>{
	  if(employee.id===id){
		employee.name = enteredDetails.name
		employee.address = enteredDetails.address
		employee.gender = enteredDetails.gender
		employee.department = enteredDetails.department
		employee.phone = enteredDetails.phone
	  }
  })
  res.json(employees);
};
