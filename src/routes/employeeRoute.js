const express = require('express');
const validate = require('express-validation');
const { createEmployee,updateEmployee } = require('../validations');
const employeeController = require('../controllers/employeeController');

const router = express.Router();

router.get('/:id',employeeController.getEmployee);
router.get('/', employeeController.getEmployees);
router.delete('/:id',employeeController.deleteEmployee);
router.post('/', validate(createEmployee), employeeController.createEmployee)
router.put('/:id', validate(updateEmployee), employeeController.updateEmployee)


module.exports = router;