const app = require('./config/express');

app.use(( err, req, res, next ) => {
    res.locals.error = err;
    if (err.status >= 100 && err.status < 600)
      res.status(err.status);
    else
      res.status(500);
    res.render('error');
  });

app.listen(3000, () => console.info(`server started on port 3000`));
